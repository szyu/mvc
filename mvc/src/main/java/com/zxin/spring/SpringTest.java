package com.zxin.spring;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.zxin.spring.di.OutputHelper;

public class SpringTest {
	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml"); 
		HelloWorld obj = (HelloWorld) context.getBean("helloBean");
		obj.printHello();
	}
	
	
	@Test
	public void diTest(){
		ApplicationContext context = new ClassPathXmlApplicationContext("classpath:com/zxin/spring/di/spring-di.xml");
		OutputHelper outputHelper = (OutputHelper)context.getBean("outputHelper");
		outputHelper.generateOutput();
	}
}

	
