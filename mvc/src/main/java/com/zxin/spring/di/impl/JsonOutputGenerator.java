package com.zxin.spring.di.impl;

import com.zxin.spring.di.IOutputGenerator;

public class JsonOutputGenerator implements IOutputGenerator {

	@Override
	public void generateOutput() {
		System.out.println("Json Output Generator");
	}

}
