package com.zxin.spring.di;

public interface IOutputGenerator
{
	public void generateOutput();
}
