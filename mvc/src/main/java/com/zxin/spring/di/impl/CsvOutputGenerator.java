package com.zxin.spring.di.impl;

import com.zxin.spring.di.IOutputGenerator;

public class CsvOutputGenerator implements IOutputGenerator {

	@Override
	public void generateOutput() {
		System.out.println("Csv Output Generator");
	}

}
