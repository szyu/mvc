package com.zxin.demo.dao;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.log4j.Logger;

import com.zxin.demo.data.User;
import com.zxin.demo.mapper.UserMapper;

public class ItemDao {
	private static Logger logger = Logger.getLogger(ItemDao.class);
	
	public static void main(String[] args) {
		try {
			String resource = "mybatis-config.xml";
			InputStream inputStream = Resources.getResourceAsStream(resource);
			SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
			SqlSession sqlSession = sqlSessionFactory.openSession();
			UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
			try {
				System.err.println(userMapper.getClass().getName());
				
				s();
//				org.mybatis.generator.internal.DefaultCommentGenerator
//				 com.zxin.MyCommentGenerator
				User user = userMapper.fetchUser(new int[]{1,2,3,4,5}  );
			} catch (Exception e) {
				e.printStackTrace();
			}
//			Items item = new Items();
//			item.setName("1");
//			item.setPrice(222);
//			//执行插入操作
//			sqlSession.insert("com.zxin.demo.insertItems", item);
//			
//			logger.error(item.getId());
//			sqlSession.delete("com.zxin.demo.deleteItems",item.getId()-3);
//			sqlSession.update("com.zxin.demo.updateItems",item.getId()-2);
			// 提交事务
			sqlSession.commit();

			// 释放资源
			sqlSession.close();

			logger.info("");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void s(){
		System.err.println(0/0);
	}
	
}
