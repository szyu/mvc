package com.zxin.demo.mapper;

import com.zxin.demo.data.User;

public interface UserMapper {
	
	public User fetchUser(int[] id) throws Exception; 
}
